const all = document.body.getElementsByTagName('*')

const persianNumbers = [
	/۰/g,
	/۱/g,
	/۲/g,
	/۳/g,
	/۴/g,
	/۵/g,
	/۶/g,
	/۷/g,
	/۸/g,
	/۹/g,
]

const arabicNumbers = [
	/٠/g,
	/١/g,
	/٢/g,
	/٣/g,
	/٤/g,
	/٥/g,
	/٦/g,
	/٧/g,
	/٨/g,
	/٩/g,
]

const fixNumbers = str => {
	if (typeof str === 'string') {
		for (let i = 0; i < 10; i++) {
			str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i)
		}
	}
	return str
}

for (let i = 0, max = all.length; i < max; i++) {
	const style = window.getComputedStyle(all[i])
	let count = 0
	if (style.position === 'absolute') {
		if (style.left !== 'auto' || style.top !== 'auto') {
			const left = parseInt(style.left)
			const top = parseInt(style.top)
			if (left < 0 || top < 0) {
				if (parseInt(fixNumbers(all[i].innerText))) {
					all[i].style.setProperty('display', 'none', 'important')
					count += 1
					if (count > 20) {
						break
					}
				}
			}
		}
	}
}
